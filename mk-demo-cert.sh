#!/usr/bin/env bash

CLIENT="DemoClient"

. ./.env

cd pki || exit 1

cp ../.env.pki .env

sudo rm -rf demoCA ca.* "${CLIENT}.*" "${NGINX_HOSTNAME}.*"

./gen-ca 'My Nginx CA'
./mk-csr --server --host ${NGINX_HOSTNAME} ${NGINX_HOSTNAME}
./sign --server ${NGINX_HOSTNAME}.csr
sudo chown 90000:90000 ${NGINX_HOSTNAME}.key ${NGINX_HOSTNAME}.crt

./mk-csr --client "${CLIENT}"
./sign --client "${CLIENT}.csr"

