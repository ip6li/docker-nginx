FROM alpine:latest

ARG MODSEC
ARG NGINX_HOSTNAME
ARG BORINGSSL

RUN apk add --no-cache \
        ca-certificates \
        libuuid \
        apr \
        apr-util \
        libjpeg-turbo \
        icu \
        icu-libs \
        geoip \
        pcre \
        bash \
        curl \
        libxml2

RUN set -x && \
    apk --no-cache add -t .build-deps \
        go \
        cmake \
        make \
        wget \
        git \
        automake \
        autoconf \
        zlib-dev \
        geoip-dev \
        apache2-dev \
        apr-dev \
        apr-util-dev \
        build-base \
        icu-dev \
        libjpeg-turbo-dev \
        linux-headers \
        gperf \
        pcre-dev \
        python3 \
        libtool \
        curl-dev \
        libxml2-dev

ADD build-nginx /usr/src/build-nginx
RUN chmod 755 /usr/src/build-nginx

RUN cd /usr/src && ./build-nginx

RUN apk del .build-deps && \
        rm -rf /tmp/*

RUN mkdir -p  /var/run/nginx && chown 90000:90000 /var/run/nginx \
  && mkdir -p  /usr/local/nginx && chown 90000:90000 /usr/local/nginx

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

