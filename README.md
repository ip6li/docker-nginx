# Nginx latest

A dockerized Nginx built from Nginx source code with http/3 support.
OpenSSL was replaced by BoringSSL.

# Sources

https://nginx.org/en/docs/quic.html

# Config

See .env.example for an example .env file.

If you want to change some parameters of docker-compose.yml file, please add
a file docker-compose.override.yml with new/changed parameters.

# Modsecurity

If do not want modsecurity, then set MODSEC=0 in .env before docker-compose build.

# Healthcheck

You should consider to add a file conf/vhosts/nginx_status.conf with following content:

```
server {
    listen       127.0.0.1:8085;
    server_name  localhost;

    location /nginx_status {
        stub_status on;
        access_log   off;
        allow 127.0.0.1;
        deny all;
    }
}
```

This file enables healthcheck for Docker.

# TLS

Get demo PKI with

```
git submodule update --init --recursive
```

